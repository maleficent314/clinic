package edu.ib.clinic;

import edu.ib.clinic.dao.*;
import edu.ib.clinic.service.DoctorService;
import edu.ib.clinic.service.PatientService;
import edu.ib.clinic.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DBMockData {

    private DoctorService doctors;
    private PatientService patients;
    private VisitService visits;

    @Autowired
    public DBMockData(DoctorService doctors, PatientService patients, VisitService visits) {
        this.doctors = doctors;
        this.patients = patients;
        this.visits = visits;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() {
        if (doctors.findAll().iterator().hasNext() || patients.findAll().iterator().hasNext()) {
            System.out.println("Database has already filled");
            return;
        }

        Patient patient1 = new Patient("Georgie", Integer.toString("pass1".hashCode()), "George", "Clooney");
        Patient patient2 = new Patient("Edward123", Integer.toString("pass2".hashCode()), "Edward", "Norton");
        Doctor doctor1 = new Doctor("Przemysław", "Przemyk", Institution.WROCLAW, Specialization.CARDIOLOGIST);
        Doctor doctor2 = new Doctor("Grzegorz", "Nowak", Institution.WROCLAW, Specialization.DENTIST);
        Doctor doctor3 = new Doctor("John", "Kowalski", Institution.WROCLAW, Specialization.LARYNGOLOGIST);
        Doctor doctor4 = new Doctor("Przemysław", "Kamyk", Institution.WROCLAW, Specialization.ORTHOPAEDIST);
        Doctor doctor5 = new Doctor("Andrzej", "Slimak", Institution.WARSAW, Specialization.CARDIOLOGIST);
        Doctor doctor6 = new Doctor("Marian", "Brzechwa", Institution.WARSAW, Specialization.DENTIST);
        Doctor doctor7 = new Doctor("Radosław", "Kotarski", Institution.WARSAW, Specialization.LARYNGOLOGIST);
        Doctor doctor8 = new Doctor("Katarzyna", "Kamyk", Institution.WARSAW, Specialization.ORTHOPAEDIST);
        Doctor doctor9 = new Doctor("Joanna", "Przemyk", Institution.POZNAN, Specialization.CARDIOLOGIST);
        Doctor doctor10 = new Doctor("Marzena", "Kowalska", Institution.POZNAN, Specialization.DENTIST);
        Doctor doctor11 = new Doctor("Ewelina", "Dryjańska", Institution.POZNAN, Specialization.LARYNGOLOGIST);
        Doctor doctor12 = new Doctor("Aleksandra", "Morska", Institution.POZNAN, Specialization.ORTHOPAEDIST);
        patients.addPatient(patient1);
        patients.addPatient(patient2);
        doctors.addDoctor(doctor1);
        doctors.addDoctor(doctor2);
        doctors.addDoctor(doctor3);
        doctors.addDoctor(doctor4);
        doctors.addDoctor(doctor5);
        doctors.addDoctor(doctor6);
        doctors.addDoctor(doctor7);
        doctors.addDoctor(doctor8);
        doctors.addDoctor(doctor9);
        doctors.addDoctor(doctor10);
        doctors.addDoctor(doctor11);
        doctors.addDoctor(doctor12);


        Visit visit1 = new Visit(patient1, doctor1, LocalDateTime.now());
        Visit visit2 = new Visit(patient2, doctor1, LocalDateTime.now());
        Visit visit3 = new Visit(patient1, doctor1, LocalDateTime.now());
        visits.addVisit(visit1);
        visits.addVisit(visit2);
        visits.addVisit(visit3);
    }
}
