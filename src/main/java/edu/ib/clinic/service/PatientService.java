package edu.ib.clinic.service;

import edu.ib.clinic.dao.Patient;
import edu.ib.clinic.dao.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    private PatientRepo patientRepo;

    @Autowired
    public PatientService(PatientRepo patientRepo) {
        this.patientRepo = patientRepo;
    }

    public Iterable<Patient> findAll() {
        return patientRepo.findAll();
    }

    public Patient addPatient(Patient patient) {
        return patientRepo.save(patient);
    }


}
