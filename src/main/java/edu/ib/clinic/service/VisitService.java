package edu.ib.clinic.service;

import edu.ib.clinic.dao.Patient;
import edu.ib.clinic.dao.Visit;
import edu.ib.clinic.dao.VisitRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VisitService {

    private VisitRepo visitRepo;

    @Autowired
    public VisitService(VisitRepo visitRepo) {
        this.visitRepo = visitRepo;
    }

    public Iterable<Visit> findAll() {
        return visitRepo.findAll();
    }

    public Visit addVisit(Visit visit) {
        return visitRepo.save(visit);
    }

    public Optional<Visit> deleteVisit(Long id){
        Optional<Visit> deletedVisit = visitRepo.findById(id);
        visitRepo.deleteById(id);
        return deletedVisit;
    }
}
