package edu.ib.clinic.service;

import edu.ib.clinic.dao.Doctor;
import edu.ib.clinic.dao.DoctorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DoctorService {

    private DoctorRepo doctorRepo;

    @Autowired
    public DoctorService(DoctorRepo doctorRepo) {
        this.doctorRepo = doctorRepo;
    }

    public Iterable<Doctor> findAll() {
        return doctorRepo.findAll();
    }

    public Doctor addDoctor(Doctor doctor) {
        return doctorRepo.save(doctor);
    }

}
