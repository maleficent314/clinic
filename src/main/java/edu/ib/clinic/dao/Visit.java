package edu.ib.clinic.dao;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "medical_visit")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinTable(name = "patient_visit", joinColumns = @JoinColumn(name = "visit_id"),
            inverseJoinColumns = @JoinColumn(name = "patient_id"))
    private Patient patient;

    @ManyToOne
    @JoinTable(name = "doctor_visit", joinColumns = @JoinColumn(name = "visit_id"),
            inverseJoinColumns = @JoinColumn(name = "doctor_id"))
    private Doctor doctor;

    private LocalDateTime visitTime;

    public Visit(Patient patient, Doctor doctor, LocalDateTime visitTime) {
        this.patient = patient;
        this.doctor = doctor;
        this.visitTime = visitTime;
    }

    public Visit() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public LocalDateTime getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(LocalDateTime visitTime) {
        this.visitTime = visitTime;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "id=" + id +
                ", patient=" + patient +
                ", doctor=" + doctor +
                ", visitTime=" + visitTime +
                '}';
    }
}
