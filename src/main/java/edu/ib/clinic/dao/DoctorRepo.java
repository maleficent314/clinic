package edu.ib.clinic.dao;

import org.springframework.data.repository.CrudRepository;

public interface DoctorRepo extends CrudRepository<Doctor, Long> {
}
