package edu.ib.clinic.dao;

public enum Specialization {
CARDIOLOGIST, DENTIST, LARYNGOLOGIST, ORTHOPAEDIST
}
