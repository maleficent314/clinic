package edu.ib.clinic.controller;

import edu.ib.clinic.dao.*;
import edu.ib.clinic.service.DoctorService;
import edu.ib.clinic.service.PatientService;
import edu.ib.clinic.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class PatientApi {

    private PatientService patientService;
    private VisitService visitService;
    private DoctorService doctorService;
    private Patient patientSaved;
    private Visit visitSaved;
    private Doctor doctorSaved;

    @Autowired
    public PatientApi(PatientService patientService, VisitService visitService, DoctorService doctorService) {
        this.patientService = patientService;
        this.visitService = visitService;
        this.doctorService = doctorService;
    }

    @GetMapping("/reg")
    public String getReg(Model model) {
        model.addAttribute("newPatient", new Patient());
        return "registration";
    }

    @PostMapping("/add")
    public String addPatient(@RequestBody @ModelAttribute Patient patient) {
        Integer hashedPassword = patient.getPassword().hashCode();
        patient.setPassword(Integer.toString(hashedPassword));
        patientService.addPatient(patient);
        System.out.println(patient);
        return "redirect:/reg";
    }

    @GetMapping("/signIn")
    public String sign(Model model) {
        model.addAttribute("newCredentials", new Credentials());
        return "login";
    }

    @GetMapping("/trial")
    public String trial(@ModelAttribute Credentials credentials, Model model) {
        String login = credentials.getLogin();
        String password = Integer.toString(credentials.getPassword().hashCode());

        List<Patient> patients = (List<Patient>) patientService.findAll();
        List<Visit> visitsAll = (List<Visit>) visitService.findAll();

        for (Patient p :
                patients) {

            if (login.equals(p.getLogin()) && password.equals(p.getPassword())) {
                model.addAttribute("idPatient", p.getId());
                model.addAttribute("login", p.getLogin());
                model.addAttribute("name", p.getName());
                model.addAttribute("surname", p.getSurname());
                System.out.println("Sign in succeed");

                List<Visit> visits = new ArrayList<>();
                for (Visit v :
                        visitsAll) {
                    if (p.getLogin().equals(v.getPatient().getLogin())) {
                        visits.add(v);
                    }
                }
                model.addAttribute("visits", visits);

                patientSaved = new Patient(p.getId(), p.getLogin(), p.getPassword(), p.getName(), p.getSurname());
                System.out.println(patientSaved);
                return "profil";
            }
        }
        System.out.println("Wrong password or login given.");
        return "redirect:/signIn";
    }

    @RequestMapping("/deleteVisit/{id}")
    public String cancelVisit(@PathVariable Long id) {
        visitService.deleteVisit(id);
        return "redirect:/trialBack";
    }

    @GetMapping("/formStart")
    public String startMakeAppointment() {
        return "form1";
    }

    @RequestMapping("/addVisit")
    public String makeAppointment(@RequestParam(value = "day") int day,
                                  @RequestParam(value = "month") int month,
                                  @RequestParam(value = "year") int year,
                                  @RequestParam(value = "specialization") Specialization spacialization,
                                  @RequestParam(value = "institution") Institution institution,
                                  Model model) {

        List<Doctor> doctorsAll = (List<Doctor>) doctorService.findAll();
        for (Doctor d :
                doctorsAll) {
            if (spacialization.equals(d.getSpecialization()) && institution.equals(d.getInstitution())) {
                doctorSaved = d;
            }
        }


        LocalDate dayOfVisit = LocalDate.of(year, month, day);

        if (dayOfVisit.isAfter(LocalDate.now())) {
            LocalTime localTime = LocalTime.of(0, 0);
            LocalDateTime date = LocalDateTime.of(dayOfVisit, localTime);
            visitSaved = new Visit(patientSaved, doctorSaved, date);
            System.out.println(visitSaved);

            List hourListAll = new ArrayList();
            LocalTime i = localTime.plusHours(9);
            while (i.isBefore(LocalTime.of(20, 0))) {
                hourListAll.add(i);
                i = i.plusMinutes(15);
            }

            List<Visit> visitListAll = (List<Visit>) visitService.findAll();
            for (Visit vis :
                    visitListAll) {

                if (visitSaved.getDoctor().getInstitution() == vis.getDoctor().getInstitution()
                        && visitSaved.getDoctor().getSpecialization() == vis.getDoctor().getSpecialization()
                        && visitSaved.getVisitTime().getYear() == vis.getVisitTime().getYear()
                        && visitSaved.getVisitTime().getMonth() == vis.getVisitTime().getMonth()
                        && visitSaved.getVisitTime().getDayOfMonth() == vis.getVisitTime().getDayOfMonth()) {

                    hourListAll.remove(vis.getVisitTime().toLocalTime());
                }
            }
            System.out.println(visitSaved);

            model.addAttribute("hourList", hourListAll);
            return "form2";
        } else return "form1";

    }

    @RequestMapping("/addVisitNext")
    public String makeAppointmentNext(@RequestParam(value = "hourChosen") LocalTime hour, Model model) {

        int year = visitSaved.getVisitTime().getYear();
        int month = visitSaved.getVisitTime().getMonthValue();
        int day = visitSaved.getVisitTime().getDayOfMonth();
        LocalDate dayOfVisit = LocalDate.of(year, month, day);

        LocalDateTime date = LocalDateTime.of(dayOfVisit, hour);
        visitSaved = new Visit(patientSaved, doctorSaved, date);
        visitService.addVisit(visitSaved);

        model.addAttribute("specialization", visitSaved.getDoctor().getSpecialization());
        model.addAttribute("nameDoc", visitSaved.getDoctor().getName());
        model.addAttribute("surnameDoc", visitSaved.getDoctor().getSurname());
        model.addAttribute("institution", visitSaved.getDoctor().getInstitution());
        model.addAttribute("date", visitSaved.getVisitTime().toLocalDate());
        model.addAttribute("time", visitSaved.getVisitTime().toLocalTime());

        return "sumUp";
    }

    @GetMapping("/trialBack")
    public String trial2(Model model) {
        model.addAttribute("idPatient", patientSaved.getId());
        model.addAttribute("login", patientSaved.getLogin());
        model.addAttribute("name", patientSaved.getName());
        model.addAttribute("surname", patientSaved.getSurname());
        List<Visit> visits = new ArrayList<>();
        List<Visit> visitsAll = (List<Visit>) visitService.findAll();

        for (Visit v :
                visitsAll) {
            if (patientSaved.getLogin().equals(v.getPatient().getLogin())) {
                visits.add(v);
            }
        }
        model.addAttribute("visits", visits);
        return "profil";
    }


}
