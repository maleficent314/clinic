package edu.ib.clinic.controller;

import edu.ib.clinic.dao.Doctor;
import edu.ib.clinic.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DoctorApi {

    private DoctorService doctorService;

    @Autowired
    public DoctorApi(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/specialist")
    public String getSpecialists(Model model) {

        List<Doctor> doctorsAll = (List<Doctor>) doctorService.findAll();
        model.addAttribute("doctors", doctorsAll);
        return "specialists";
    }
}
